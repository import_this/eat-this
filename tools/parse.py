import requests
import psycopg2


def get_cafe_list():
    r = requests.get('https://search-maps.yandex.ru/v1/', params={
        'text': 'кафе',
        'type': 'biz',
        'lang': 'ru_RU',
        'apikey': '52dc10b8-0fa7-49df-a21b-c79a48a766df',
        'bbox': '41.881003,44.993245~42.084936,45.105196',
        'results': '500'
    })
    return r.json()


def connect_cursor():
    conn = psycopg2.connect(dbname='eat_this', user='eat_this', password='eat_this', host='127.0.0.1')
    return conn


def save_to_db(cafe_list, cursor):
    for cafe in cafe_list:
        cursor.execute('INSERT INTO familiarity_api_places(name, address, location, description) '
                       'VALUES(%s, %s, ST_SetSRID(ST_MakePoint(%s, %s),4326), %s)', cafe)
    cursor.connection.commit()


def main():
    cafe_list = []
    for cafe in get_cafe_list()['features']:
        description = ';'.join([cat['name'] for cat in cafe['properties']['CompanyMetaData']['Categories']])
        name = cafe['properties']['CompanyMetaData']['name']
        address = cafe['properties']['CompanyMetaData']['address']
        cafe_list.append((name, address, cafe['geometry']['coordinates'][0],
              cafe['geometry']['coordinates'][1], description))

    conn = connect_cursor()
    save_to_db(cafe_list, conn.cursor())


if __name__ == '__main__':
    main()

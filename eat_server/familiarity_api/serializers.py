from .models import *
from rest_framework import serializers


class PointSerializer(serializers.Serializer):
    longitude = serializers.CharField(source='x')
    latitude = serializers.CharField(source='y')


class GuestSerializer(serializers.ModelSerializer):

    class Meta:
        model = Guest
        fields = ('id', 'name', 'birthdate', 'gender', 'place', 'avatar')


class PlacesSerializer(serializers.ModelSerializer):
    guests_count = serializers.ReadOnlyField()
    guests = GuestSerializer(many=True)
    id = serializers.ReadOnlyField()

    class Meta:
        model = Places
        fields = ('id', 'name', 'description', 'address', 'location', 'guests_count', 'guests', 'has_action', 'action_text')

    location = PointSerializer()


class PlaceChatSerializer(serializers.ModelSerializer):
    from_guest = GuestSerializer()

    class Meta:
        model = PlaceChat
        fields = ('id', 'message', 'from_guest', 'place', 'timestamp')


class GuestChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuestChat
        fields = ('id', 'message', 'from_guest', 'to_guest', 'timestamp')


class VisitStatSerializer(serializers.ModelSerializer):
    guest = GuestSerializer()
    class Meta:
        model = VisitStat
        fields = ('guest', 'place', 'visit_count', 'last_visit')

from django.contrib.auth.models import User
from django.contrib.gis.db import models


class Places(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=1024, null=True)
    address = models.CharField(max_length=512)
    location = models.PointField(geography=True)
    has_action = models.BooleanField(default=False, blank=True)
    action_text = models.TextField(null=True, blank=True)

    @property
    def guests_count(self):
        return len(self.guests.all())

    @property
    def guests(self):
        return list([guest.id for guest in self.guests.all()])

    def __str__(self):
        return f'{self.name} ({self.address})'


class Guest(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    place = models.ForeignKey(Places, on_delete=models.CASCADE, null=True, blank=True, related_name='guests')
    birthdate = models.DateField()
    name = models.CharField(max_length=32)
    gender = models.BooleanField()
    avatar = models.CharField(max_length=256, default='')


class GuestChat(models.Model):
    from_guest = models.ForeignKey(Guest, related_name='fromg', on_delete=models.CASCADE)
    to_guest = models.ForeignKey(Guest, related_name='tog', on_delete=models.CASCADE)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.message


class PlaceChat(models.Model):
    from_guest = models.ForeignKey(Guest, on_delete=models.CASCADE)
    place = models.ForeignKey(Places, on_delete=models.CASCADE)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)


class VisitStat(models.Model):
    guest = models.ForeignKey(Guest, on_delete=models.CASCADE)
    place = models.ForeignKey(Places, on_delete=models.CASCADE)
    visit_count = models.IntegerField(default=0)
    last_visit = models.DateTimeField(null=True)

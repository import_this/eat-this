from django.contrib.gis import admin
from .models import *

admin.site.register(Places, admin.OSMGeoAdmin)
admin.site.register(Guest)
admin.site.register(VisitStat)
admin.site.register(GuestChat)
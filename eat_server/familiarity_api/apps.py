from django.apps import AppConfig


class FamiliarityApiConfig(AppConfig):
    name = 'familiarity_api'

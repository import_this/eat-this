from rest_framework import viewsets, views, permissions
from datetime import datetime
from django.db.models import Q
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from rest_framework.decorators import action
from rest_framework.response import Response
from django.views.decorators.http import require_POST, require_GET
from django.contrib.gis.geos import Point, Polygon
from django.shortcuts import get_object_or_404
from django.middleware.csrf import _get_new_csrf_token
from .serializers import *
from .models import *


class VisitStatViewSet(viewsets.ModelViewSet):
    serializer_class = VisitStatSerializer

    def get_queryset(self):
        params = self.request.query_params
        if 'place' in params:
            return VisitStat.objects.filter(place=params['place'])
        return VisitStat.objects.all()


class GuestViewSet(viewsets.ModelViewSet):
    serializer_class = GuestSerializer

    def get_queryset(self):
        return Guest.objects.all()


class UserLocationViewSet(viewsets.ModelViewSet):
    serializer_class = PlacesSerializer

    @action(methods=['get'], detail=False)
    def visited(self, request):
        places = Places.objects.filter(pk__in=VisitStat.objects.all().values_list('place', flat=True))
        serializer = PlacesSerializer(places, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        params = self.request.query_params

        if all(name in params for name in ['lat', 'lng', 'rad']):
            lat = params['lat']
            lon = params['lng']
            radius = params['rad']
            center = Point(float(lon), float(lat))
            return Places.objects.filter(location__distance_lte=(center, int(radius)))
        elif all(name in params for name in ['lat_ne', 'lon_ne', 'lat_sw', 'lon_sw']):
            ne = (params['lat_ne'], params['lon_ne'])
            sw = (params['lat_sw'], params['lon_sw'])
            bbox = (sw[1], sw[0], ne[1], ne[0])
            geom = Polygon.from_bbox(bbox)
            return Places.objects.filter(location__coveredby=geom)
        return Places.objects.all()


def get_crf_token(request, id):
    return JsonResponse({'token': _get_new_csrf_token()})


@require_POST
def update_location(request, id):
    guest = get_object_or_404(Guest, pk=id)
    lon = request.POST.get('lng')
    lat = request.POST.get('lat')
    center = Point(float(lon), float(lat))
    places = Places.objects.filter(location__distance_lte=(center, int(40)))
    action_places = Places.objects.filter(Q(location__distance_lte=(center, int(1000))) & Q(has_action=True))

    action = None
    if (action_places):
        action = PlacesSerializer(action_places[0]).data

    if len(places):
        if guest.place != places[0]:
            guest.place = places[0]
            guest.save()

            visit, created = VisitStat.objects.get_or_create(guest=guest, place=guest.place)
            visit.last_visit = datetime.now()
            visit.visit_count += 1
            visit.save()

        return JsonResponse({'in_place': True, 'place_id': guest.place.id, 'action': action})
    else:
        guest.place = None
        guest.save()
        return JsonResponse({'in_place': False, 'action': action})


@require_POST
def send_guest_message(request):
    from_guest_id = request.POST.get('from')
    to_guest_id = request.POST.get('to')
    message = request.POST.get('message')
    from_guest = get_object_or_404(Guest, pk=from_guest_id)
    to_guest = get_object_or_404(Guest, pk=to_guest_id)
    chat = GuestChat(from_guest=from_guest, to_guest=to_guest, message=message)
    chat.save()
    return JsonResponse({'chat_id': chat.id})


@require_POST
def send_place_message(request):
    from_guest_id = request.POST.get('from')
    place_id = request.POST.get('place')
    from_guest = get_object_or_404(Guest, pk=from_guest_id)
    place = get_object_or_404(Places, pk=place_id)
    message = request.POST.get('message')
    chat = PlaceChat(from_guest=from_guest, place=place, message=message)
    chat.save()
    return JsonResponse({'chat_id': chat.id})


@require_GET
def get_place_chat(request, place_id):
    place = get_object_or_404(Places, pk=place_id)
    chats = PlaceChat.objects.filter(place=place).order_by('id')
    return JsonResponse(PlaceChatSerializer(chats, many=True).data, safe=False)


@require_GET
def get_guests_chat(request, from_user_id, to_user_id):
    from_user = get_object_or_404(Guest, pk=from_user_id)
    to_user = get_object_or_404(Guest, pk=to_user_id)
    chats = GuestChat.objects.filter(Q(from_guest=from_user, to_guest=to_user) |
                                     Q(from_guest=to_user, to_guest=from_user)).order_by('id')
    return JsonResponse(GuestChatSerializer(chats, many=True).data, safe=False)


@require_GET
def get_history_chats(request, guest_id):
    guest = get_object_or_404(Guest, pk=guest_id)
    chats = GuestChat.objects.filter(Q(from_guest=guest) | Q(to_guest=guest))
    guests = Guest.objects.filter(
                                  (Q(pk__in=chats.values_list('from_guest', flat=True)) |
                                  Q(pk__in=chats.values_list('to_guest', flat=True)) ) &
                                  ~ Q(pk=guest_id)
                                  )
    return JsonResponse(GuestSerializer(guests, many=True).data, safe=False)

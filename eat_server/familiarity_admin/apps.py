from django.apps import AppConfig


class FamiliriatyAdminConfig(AppConfig):
    name = 'familiarity_admin'

"""eat_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from django.urls import path, re_path

from familiarity_api import views
from familiarity_admin import views as adminViews
from familiarity_front import views as frontViews

router = routers.DefaultRouter()
router.register(r'userlocations', views.UserLocationViewSet, base_name='UserLocation')
router.register(r'guests', views.GuestViewSet, base_name='Guests')
router.register(r'visits', views.VisitStatViewSet, base_name='VisitStat')

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    re_path(r'^placeadmin/.*$', adminViews.place_view),
    re_path(r'^point/.*$', frontViews.place_view),
    url(r'^api-auth/', include('rest_framework.urls')),
    path(r'guests/<int:id>/updateLocation/', views.update_location),
    path(r'guests/<int:id>/get_crf_token', views.get_crf_token),
    path(r'guest_chat/', views.send_guest_message),
    path(r'place_chat/', views.send_place_message),
    path(r'place_chat/<int:place_id>/', views.get_place_chat),
    path(r'guest_chat/from<int:from_user_id>/to<int:to_user_id>/', views.get_guests_chat),
    path(r'history_chat/<int:guest_id>/', views.get_history_chats),
]

from django.apps import AppConfig


class FamiliarityFrontConfig(AppConfig):
    name = 'familiarity_front'

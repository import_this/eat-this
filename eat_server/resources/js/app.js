import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'


window._ = require('lodash');
window.axios = require('axios');
window.Vue = Vue;
Vue.use(Vuetify);

Vue.use(VueRouter);
import dashboard from './components/dashboard';
import settingsForm from './components/settings';
import customersList from './components/customers';


const routes = [
  { path: '/dashboard', component: dashboard, name: 'dashboard' },
  { path: '/settings', component: settingsForm, name: 'settings' },
  { path: '/customers', component: customersList, name: 'customers' },
]

// 3. Создаём экземпляр маршрутизатора и передаём маршруты в опции `routes`
// Вы можете передавать и дополнительные опции, но пока не будем усложнять.
const router = new VueRouter({
  mode: 'history',
  base: '/placeadmin/',
  routes // сокращённая запись для `routes: routes`
})

const app = new Vue({
  router,
  data:function(){
    return {
      drawer: false,
    }
  }
}).$mount('#app')

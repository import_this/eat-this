FROM ubuntu:bionic

MAINTAINER Alexander Medvidov <midvikus@gmail.com>

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends \
		gcc \
		gettext \
		postgresql-client libpq-dev \
		python3-pip \
		python3-setuptools \
		python3-dev \
		python3-gdal \
		libjpeg8-dev \
		git \
		build-essential \
		libssl-dev \
		libffi-dev \
		postgresql-10-postgis-2.4 \
		postgresql-10-postgis-2.4-scripts \
	--no-install-recommends

RUN mkdir /opt/meetpoint

COPY requirements.txt /opt/meetpoint

COPY uwsgi.ini /opt/

RUN pip3 install --upgrade pip

RUN pip3 install wheel

RUN pip3 install uwsgi

RUN pip3 install -r /opt/meetpoint/requirements.txt
